from django.db import models
from django_prometheus.models import ExportModelOperationsMixin


class Currency(ExportModelOperationsMixin('currency'), models.Model):
    """Currency"""

    name = models.CharField('Name', max_length=64)
    char_code = models.CharField(max_length=3, db_index=True)
    value = models.FloatField('Value')
    date_update = models.DateField('Updated')
    nominal = models.IntegerField('Nominal', default=1)
    nominal_value = models.FloatField('Nominal Value', default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Currencies'
        unique_together = ['char_code', 'date_update']
