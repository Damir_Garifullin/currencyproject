from rest_framework import serializers

from .models import Currency


class CurrencySerializer(serializers.ModelSerializer):
    """Currency"""

    class Meta:
        model = Currency
        fields = ('char_code', 'name', 'value')
