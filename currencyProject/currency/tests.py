import re

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from . import tasks


class CurrencyTestCase(APITestCase):
    ok = status.HTTP_200_OK
    not_found = status.HTTP_404_NOT_FOUND

    def setUp(self):
        tasks.update_currency()

    def test_correct_uppercase(self):
        response = self.client.get(reverse('currency-detail', kwargs={'char_code': 'EUR'}), format='json')
        self.assertEqual(response.status_code, self.ok)

    def test_correct_lowercase(self):
        response = self.client.get(reverse('currency-detail', kwargs={'char_code': 'eur'}), format='json')
        self.assertEqual(response.status_code, self.ok)

    def test_incorrect_alpha(self):
        response = self.client.get(reverse('currency-detail', kwargs={'char_code': 'fdghfdhfdghdfdhg'}), format='json')
        self.assertEqual(response.status_code, self.not_found)

    def test_incorrect_numbers(self):
        response = self.client.get(reverse('currency-detail', kwargs={'char_code': '123'}), format='json')
        self.assertEqual(response.status_code, self.not_found)

    def test_euro_name(self):
        response = self.client.get(reverse('currency-detail', kwargs={'char_code': 'EUR'}), format='json')
        self.assertEqual(response.data['name'], 'Euro')
        self.assertEqual(response.status_code, self.ok)

    def test_dollar_name(self):
        response = self.client.get(reverse('currency-detail', kwargs={'char_code': 'USD'}), format='json')
        self.assertEqual(response.data['name'], 'US Dollar')
        self.assertEqual(response.status_code, self.ok)

    def test_list(self):
        response = self.client.get(reverse('currency-list'), format='json')
        self.assertEqual(response.status_code, self.ok)

    def test_correct_converter(self):
        curr_from = 'EUR'
        curr_to = 'USD'
        response = self.client.get(reverse('currency-convert', kwargs={'curr_from': curr_from, 'curr_to': curr_to}),
                                   format='json')
        prog = re.compile('^[0-9]+\.[0-9]{2}$')
        self.assertTrue(bool(prog.match(response.data['value'])))
        self.assertEqual(response.data['from'], curr_from)
        self.assertEqual(response.data['to'], curr_to)
        self.assertEqual(response.status_code, 200)

    def test_incorrect_converter_first_param(self):
        response = self.client.get(reverse('currency-convert', kwargs={'curr_from': 'asdasd', 'curr_to': 'eur'}),
                                   format='json')
        self.assertEqual(response.status_code, self.not_found)

    def test_incorrect_converter_second_param(self):
        response = self.client.get(reverse('currency-convert', kwargs={'curr_from': 'usd', 'curr_to': 'ezcar'}),
                                   format='json')
        self.assertEqual(response.status_code, self.not_found)

    def test_incorrect_converter_both_param(self):
        response = self.client.get(reverse('currency-convert', kwargs={'curr_from': 'uasdsd', 'curr_to': 'ezcar'}),
                                   format='json')
        self.assertEqual(response.status_code, self.not_found)
