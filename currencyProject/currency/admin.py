from django.contrib import admin

# Register your models here.
from currency.models import Currency

admin.site.register(Currency)
