from django.urls import path

from . import views

urlpatterns = [
    path('currency/', views.CurrencyViewSet.as_view({'get': 'list'}), name='currency-list'),
    path('currency/<str:char_code>/', views.CurrencyViewSet.as_view({'get': 'retrieve'}), name='currency-detail'),
    path('currency/<str:curr_from>/<str:curr_to>/', views.CurrencyAPIView.as_view(), name='currency-convert'),
]
