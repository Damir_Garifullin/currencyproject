from rest_framework import viewsets
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Currency
from .serializers import CurrencySerializer


class CurrencyViewSet(viewsets.ViewSet):
    """
        Exchange rate for Russian Ruble

        Available currencies:
        AMD, AUD, AZN, BGN, BRL, BYN, CAD, CHF, CNY, CZK, DKK,
        EUR, GBP, HKD, HUF, INR, JPY, KGS, KRW, KZT, MDL, NOK,
        PLN, RON, SEK, SGD, TJS, TMT, TRY, UAH, USD, UZS, XDR, ZAR

        return json object like {'char_code': , 'name': , 'value': }

        raise Http404 if don't find any currency
    """

    def list(self, request):
        queryset = Currency.objects.all()
        last_date = queryset.last().date_update
        currency_list = queryset.filter(date_update=last_date)
        serializer = CurrencySerializer(currency_list, many=True)
        return Response(serializer.data)

    def retrieve(self, request, char_code=''):
        queryset = Currency.objects.all()
        last_date = queryset.last().date_update
        upper_char_code = char_code.upper()
        currency = get_object_or_404(queryset, char_code=upper_char_code, date_update=last_date)
        serializer = CurrencySerializer(currency)
        return Response(serializer.data)


class CurrencyAPIView(APIView):
    """
        Currencies Converter

        return json object like {'from': , 'to': , 'value': }

        raise 404 if don't find any currency
    """

    def get(self, request, curr_from, curr_to):
        queryset = Currency.objects.all()
        last_date = queryset.last().date_update
        curr_from = curr_from.upper()
        curr_to = curr_to.upper()
        currency_from = get_object_or_404(queryset, char_code=curr_from, date_update=last_date)
        currency_to = get_object_or_404(queryset, char_code=curr_to, date_update=last_date)
        value = currency_from.value / currency_to.value
        result_dict = {
            'from': currency_from.char_code,
            'to': currency_to.char_code,
            'value': "%.2f" % value,
        }
        return Response(result_dict)
