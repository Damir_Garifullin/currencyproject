# Generated by Django 3.1.1 on 2020-09-25 10:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('currency', '0003_auto_20200924_1612'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='currency',
            unique_together={('char_code', 'date_update')},
        ),
    ]
