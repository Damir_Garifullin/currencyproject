# Generated by Django 3.1.1 on 2020-09-21 17:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('currency', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='currency',
            options={'verbose_name_plural': 'Currencies'},
        ),
        migrations.AddField(
            model_name='currency',
            name='nominal',
            field=models.IntegerField(default=1, verbose_name='Nominal'),
        ),
        migrations.AddField(
            model_name='currency',
            name='nominal_value',
            field=models.FloatField(default=0, verbose_name='Nominal Value'),
        ),
    ]
